### Notes

Merci de m'avoir invité sur votre dépôt. Plusieurs conseils pour vous:

- Organiser votre dépot correctement. Le Readme (ce fichier) doit etre un point d'entré et donner toutes
les informations dont vous avez besoin et faire référence avec lien vers le code ou autre.
- Organiser votre dépôt: Convenez d'une architecture de dossier par exemple:
```sh
- Code
        |_ led
        |_ gps
- Doc
        |_ linkit one
        |_ site_web
- ...
```
- Ouvrez des issues que vous pouvez vous assignez entre vous ou à vous même. (je vais en ouvrir une pour ce qui est dit au dessus)
quand vous avez fini, vous pouvez fermer l'issue.

[tahiry]