/*
 WiFi Web Server
 A simple web server that a repeated counter
 Change the macro WIFI_AP, WIFI_PASSWORD and WIFI_AUTH accordingly.
 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 modified 20 Aug 2014
 by MediaTek Inc.
 */
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#define WIFI_AP "RT-WIFI-Guest"
#define WIFI_PASSWORD "wifirtguest"
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WL_MAC_ADDR_LENGTH 6
int nbrWifi = 0;
char addrMac[6][]= {
  {"RT-WF-Admin","RT-WF-PJD","RT-WF-RC","RT-WF-SS","RT-WF-TD1","RT-WF-TD3"},

{"80:2a:a8:59:5c:68","80:2a:a8:49:ad:26","80:2a:a8:49:ad:2e","80:2a:a8:49:af:2e","80:2a:a8:46:ce:a5","80:2a:a8:49:ac:55"}
};

LWiFiServer server(80);
void setup()
{
  LWiFi.begin();
  Serial.begin(115200);
  nbrWifi = LWiFi.scanNetworks();

  int i =0;
  for (i =0; i < 3 ; i++){
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" ");
  
  printScan(nbrWifi);
  for (i = 0; i < nbrWifi; i ++) {
    // keep retrying until connected to AP
    Serial.println("Connecting to AP");
    while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
    {
      delay(1000);
    }
    printWifiStatus();
    Serial.println("Start Server");
    server.begin();
    Serial.println("Server Started");
    //printBSSID();
  }
}
int loopCount = 0;
void loop()
{
  // put your main code here, to run repeatedly:
  delay(500);
  loopCount++;
  LWiFiClient client = server.available();
  if (client)
  {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())
      {
        // we basically ignores client request, but wait for HTTP request end
        int c = client.read();
        Serial.print((char)c);
        if (c == '\n' && currentLineIsBlank)
        {
          Serial.println("send response");
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // output the value of each analog input pin
          client.print("loop");
          client.print(" is ");
          client.print(loopCount);
          client.println("<br />");
          client.println("</html>");
          client.println();
          break;
        }
        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(500);
    // close the connection:
    Serial.println("close connection");
    client.stop();
    Serial.println("client disconnected");
  }
}
void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  /*IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());*/
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
void printBSSID(){
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("AP's MAC Address is: ");
  for(int i = 0; i < WL_MAC_ADDR_LENGTH; ++i)
  {
    Serial.print(BSSID[i], HEX);
    Serial.print(":");
  }
  Serial.println();
}
void printScan( int nb) {
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  int i =0;
  for (i =0; i < nb ; i++){
    Serial.print("Reseau N ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.println(LWiFi.SSID(i));
    Serial.println(LWiFi.RSSI(i));
    Serial.print(BSSID[i], HEX);
    Serial.print(":");
}

