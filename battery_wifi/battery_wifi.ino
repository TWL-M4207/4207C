/*
 WiFi Web Server
 A simple web server that a repeated counter
 Change the macro WIFI_AP, WIFI_PASSWORD and WIFI_AUTH accordingly.
 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 modified 20 Aug 2014
 by MediaTek Inc.
 */
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#define WIFI_RT_GUEST_AP "RT-WIFI-Guest"
#define WIFI_RT_GUEST_PASSWORD "wifirtguest"
#define WIFI_RT_GUEST_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WIFI_EDU_AP "eduspot"
#define WIFI_EDU_PASSWORD ""
#define WIFI_EDU_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WL_MAC_ADDR_LENGTH 6
#define ROW_COUNT(array)    (sizeof(array) / sizeof(*array))
#include <LBattery.h>
#include <Wire.h>
#include "rgb_lcd.h"
#define BUZZER_PIN 4

char buff[256];
rgb_lcd lcd;
int white[3] = {255,255,255};
int blue[3] = {0,0,255};
int  red [3] = {255,0,0};

char* rt_guest_salle[6][2] = {
  {"Administration RT","80:2A:A8:5A:5C:68:"},
  {"Proj doc","80:2A:A8:4A:AD:26:"},
  {"Res cabl","80:2A:A8:4A:AD:2E:"},
  {"Sig sys","80:2A:A8:4A:AF:2E:"},
  {"TD1","80:2A:A8:47:CE:A5:"},
  {"TD3","80:2A:A8:4A:AC:55:"}
};
char* eduspot_salle[5][2] = {
  {"Administration RT"," 0:1D:A2:D8:38:B1:"},
  //{"La salle TD2","0:1D:A2:D8:3D:E1:"},
  //{"La salle Signaux et systÃ¨mes","0:14:A8:39:2C:B6:"},
  {"Proj doc"," 0:14:6A:F0:45:51:"},
  {"Res cabl"," 0:14:6A:F0:42:21:"},
  {"Micro inf prog"," 0:14:6A:F0:41:31:"},
  {"TD1"," 0:1D:A2:D8:3D:E1:"},
  //{"La salle Genie informatique","0:1D:A2:D8:3D:E1:"}
};
int i =0;
int j =0;
char addr_mac[17];


LWiFiServer server(80);

void setup()
{
  LWiFi.begin();
  Serial.begin(115200);
  pinMode(BUZZER_PIN, OUTPUT);
  lcd.begin(16,2);
  lcd.setRGB(white[0],white[1],white[2]);
}
void loop()
{
  // put your main code here, to run repeatedly:
  sprintf(buff,"battery = %d", LBattery.level() );
  Serial.print(buff);
  lcd.setCursor(0,0);
  lcd.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  lcd.setCursor(0,0);
  Serial.println(buff);
  delay(1000);
  if (LBattery.level() == 100){
    lcd.setRGB(blue[0],blue[1],blue[2]);
  }
  else if(LBattery.level() == 66){
    lcd.setRGB(white[0],white[1],white[2]);
  }
  else if(LBattery.level() == 33){
    lcd.setRGB(red[0],red[1],red[2]);
    digitalWrite(BUZZER_PIN,HIGH);
    delayMicroseconds(5000);
    digitalWrite(BUZZER_PIN, LOW);
    delayMicroseconds(5000);
  }
  getInfo(WIFI_EDU_AP, WIFI_EDU_AUTH, WIFI_EDU_PASSWORD);
  getInfo(WIFI_RT_GUEST_AP, WIFI_RT_GUEST_AUTH, WIFI_RT_GUEST_PASSWORD);
}
void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  // print BSSID (MAC Adr)
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("AP's MAC Address is: ");
  sprintf(addr_mac,"%2X:%2X:%2X:%2X:%2X:%2X:",BSSID[0],BSSID[1],BSSID[2],BSSID[3],BSSID[4],BSSID[5]);
  Serial.print(addr_mac);
  Serial.println();
  quelleSalle(addr_mac);
  Serial.println();
}
void getInfo(char *rssi, LWiFiEncryption auth, char *psswd)
{
  LWiFi.disconnect();
  while (0 == LWiFi.connect(rssi, LWiFiLoginInfo(auth, psswd)))
  {
    delay(1000);
  }
  printWifiStatus();
}
void quelleSalle(char *mac)
{
  for(i=0;i<ROW_COUNT(rt_guest_salle);i++){
    if(strcmp(mac,rt_guest_salle[i][1])== 0){
       Serial.println(rt_guest_salle[i][0]);
       lcd.setCursor(0,1);
       lcd.print(eduspot_salle[j][0]);
    }
  }
  
  for(j=0;j<ROW_COUNT(eduspot_salle);j++){
    if(strcmp(mac,eduspot_salle[j][1])== 0){
       Serial.println(eduspot_salle[j][0]);
       lcd.setCursor(0,1);
       lcd.print("                     ");
       lcd.setCursor(0,1);
       lcd.print(eduspot_salle[j][0]);
    }
  }
}


