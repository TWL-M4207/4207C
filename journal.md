# Bilan séance 31/01/2017
Aujourd'hui durant cette 1ere seance de tp j'ai pu commencé la tache comment 
localiser la clé.Pour cela je pense que pour localiser la clé dans le 
département rt on doit utiliser le réseau wifi de rt.Cependant le réseau wifi 
étant en roaming notre adresse ip ne change pas. Donc pour déterminer dans 
quelle classe nous sommes nous devons utilisé l'adresse MAC du routeur auquel on
est connecté mais je n'ai pas encore trouvé le moyen de déterminer l'adresse MAC
du routeur auquel le linkit one est connecté.

### Attente/Travail par le suivant
Continué la tache comment localiser la clé : - Moyen pour déterminer l'adresse 
MAC du routeur.(http://labs.mediatek.com/api/linkit-one/frames.html?frmname=topic&frmfile=index.html)

# Bilan séance 03/02/2017
L'adresse MAC du routeur est maintenant accessible, pour la localisation je pense à une triangulation, en utilisant le RSSI (la puissance d'émission de la borne wifi).
Je m'explique, on scanne toutes les bornes wifi donc on aura une liste des bornes qui sont autour de notre linkit one après on affiche chaque RSSI de chaque routeur (borne wifi) scanné.
Sur ce stade, on a une liste de routeur avec les RSSI correspondants.
Maintenant, on va faire correspondre chaque RSSI avec une distance (exemple: si RSSI = -71 bpm donc on suppose qu'on est à 6 mètres du routeur).
Il ne reste plus qu'à faire une triangulation à partir des données reçues.
Exemple : 
Liste de routeur :
    R1          -71bpm
    R2          -15bpm
    R3          -50bpm
    R4          -16bpm
Distances :
    R1 est à environ 15 m
    R2 est à 5 m
    R3 est à 10 m
    R4 est à 4,50 m
Pour la triangulation on utilisera un plan fourni par Kevin où on aura ces informations : Position du routeur et son adresse MAC.
Le code que j'ai founi task1_v2 ne contient actuellement que des lignes de code pour afficher le nombre de bornes wifi.

# Attente/Travail par le suivant
Pour la suite le prochain devrait modfier le fichier task1_v2 pour qu'on puisse lister les bornes wifi et lister aussi ses RSSI.
S'il a le temps il peut aussi commencer de travailler sur la triangulation. D'abord faire correspondre une distance à une puissance (RSSI).
Et après attaquer la triangulation, trouver un moyen de calculer où se positionne la clé. (à partir des différentes distances,...).
Au final on aura le nom de la salle comme valeur return.
Je pense à créer un genre de tableau pour stocker les coordonnées des routeurs et aussi des salles (et leurs noms).
Bon courage ;)
http://labs.mediatek.com/api/linkit-one/frames.html?frmname=topic&frmfile=index.html
RT-WF-Admin 80:2a:a8:59:5c:68
RT-WF-PJD 80:2a:a8:49:ad:26
RT-WF-RC 80:2a:a8:49:ad:2e
RT-WF-SS 80:2a:a8:49:af:2e
RT-WF-TD1 80:2a:a8:46:ce:a5
RT-WF-TD3 80:2a:a8:49:ac:55

# Bilan séance 03/02/2017
Je ne vais pas mentir: je n'ai pas pu avancer sur le projet, à cause de mon étourderie l'installation et la mise en place 
des matériels m'a pris plus de temps que prévu sur Linux.
Mais malgré ça j'ai compris le prochain objectif du projet: 
une triangulation à l'aide des routeurs (on se servira des adresses MAC et on y calculera la position avec les puissances de captation du signal.

La clé calculera les distances vis à vis de trois routeurs très proches (leurs adresses MAC communiqueront les coordonnées GPS), et avec ça nous 
saurons la position de la clé en question.



J'espère juste gagner du temps et être sûr de pouvoir prendre en main le programme. 
Je ne cacherai pas une certaine frustration vis-à-vis de moi-même.

# Bilan séance 08/02/2017
Aujour'hui j'ai pu écrire un code pour lister la liste des bornes wifi disponibles que le link it one a scanné.

### Attente/Travail par le suivant
Le suivant devra faire l'issue #3

# Bilan séance 14/02/2017
Aujourd'hui je n'ai pas pu continu sur la triangulation des réseaux wifi car le réseau rt wifi ne fonctionne pas
alors je me suis concentré sur alerter l'utilisateur lorsque la clé n'a plus de batterie via un signal sonore
pour cela j'ai utilisé un buzzer et la carte supplémentaire car j'ai utilisé une entré analogique pour le buzzer. 

# Bilan séance 21/02/2017
Commencer issue #8

### Attente/Travail par le suivant
Continuer l'issue #8

# Bilan séance 21/02/2017 #2
Continuer l'issue #8 et réussir l'issue #10 et #9

### Attente/Travail par le suivant
Continuer l'issue #8 sur le code : task1_boucle_wifi.ino. Mettre une condition pour déterminer la salle selon l'adresse MAC (rssi)

# Bilan séance 21/02/2017 #3
Erreur trouvé dans le programme:
<code>Arduino : 1.8.1 (Mac OS X), Carte : "LinkIt ONE"

<code>Attention: platform.txt du cœur 'MediaTek ARM7 EJ-S (32-bits) Boards' contiens recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}" {compiler.ar.flags} "{build.path}/{archive_file}" "{object_file}" dépassé, converti automatiquement en recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}" {compiler.ar.flags} "{archive_file_path}" "{object_file}". La mise a niveau de ce cœur est conseillée.
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
};
^
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
};
^
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
/Users/RT/4207C/Code/testReconnaissanceWifiTWL/testReconnaissanceWifiTWL.ino: In function 'void setup()':
testReconnaissanceWifiTWL:44: error: 'i' was not declared in this scope
for (i = 0; i < 8; i = i + 1){
^
testReconnaissanceWifiTWL:45: error: 'j' was not declared in this scope
for (j = 0; j < 2; j = j + 1){
^
exit status 1
too many initializers for 'char [2]'

<code>Ce rapport pourrait être plus détaillé avec
l'option "Afficher les résultats détaillés de la compilation"
activée dans Fichier -> Préférences.

J'essaye en ce moment de mettre tous les éléments dans un seul et même variable tableau.

Et malheureusement le même erreur est apparu:

<code>Arduino : 1.8.1 (Mac OS X), Carte : "LinkIt ONE"

<code>Attention: platform.txt du cœur 'MediaTek ARM7 EJ-S (32-bits) Boards' contiens recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}" {compiler.ar.flags} "{build.path}/{archive_file}" "{object_file}" dépassé, converti automatiquement en recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}" {compiler.ar.flags} "{archive_file_path}" "{object_file}". La mise a niveau de ce cœur est conseillée.
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
};
^
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:30: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
};
^
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: too many initializers for 'char [2]'
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
testReconnaissanceWifiTWL:40: error: initializer-string for array of chars is too long [-fpermissive]
exit status 1
too many initializers for 'char [2]'

<code>Ce rapport pourrait être plus détaillé avec
l'option "Afficher les résultats détaillés de la compilation"
activée dans Fichier -> Préférences.

---

Le probleme est résolu: Quand on fait un tableau de <code>char</code>, il ne faut <strong>pas oublier <code>l'asterisque</code> pour écrire <code>char(asterisque) [][2]</code></strong>. Cet oubli cause l'erreur causé juste avant.

# Bilan seance 24/02/2017
Aujourd'hui j'ai terminer l'issue numero 8,la création d'un tableau j'ai aussi pu afficher la salle a laquelle l'AP connecté 

### Attente/Travail par le suivant
Commencer a réfléchir a l'issue numero 11

# Bilan seance 24/02/2017 #2
Je me suis concentré sur comment faire avertir que la batterie est faible et AUSSI sur "Comment savoir qui a pris la clé?". Le principe est simple:
- Chaque clé a son propre identification du type **828475IDxxxxxx** (828475 correspond à RTK en ASCII decimal, ID c'est ID, et les xxxxxx c'est le numéro de clé en hexadecimal, LUI-MEME en hexadecimal)
- Celui qui récupère la clé donnera son nom et le numéro de la clé qu'il emprunte (J'emprunte le numéro A17 au nom de Marie-Sergio GERTRUDE!) et le secrétaire (ou celui qui est en charge du serveur-client) notera le nom à coté du numéro de clé et de l'identifiant correspondant (ici *828475ID413137*)

Concernant la tâche "Batterie faible", j'ai cherché un brouillon sur internet et ça pourrait servir. Voir le programme verifyBattery.ino .

# Bilan seance 28/02/2017
Aujourd'hui j'ai pas fait grand chose, voire pas du tout, je suis un peu fatigué.

### Attente/Travail par le suivant
Le suivant devra continuer le travail sur savoir qui a pris la clé, un exemple est de créer un site web php, sql. Et d'envoyer des informations dessus via des POST de php.

# Bilan séance 28/02/2017 #2
Je me concentre actuellement sur **"Comment savoir qui possède la clé"** et un forum a attiré mon attention:
<code>http://forum.arduino.cc/index.php?topic=308481.0</code>
A savoir maintenant si le shield Ethernet est disponible sur Linkit One ou pas, dans le but de pouvoir se connecter à un serveur distant via l'adresse IP attribuée à la clé. Le site formulé ci-dessus donne des informations sur comment se connecter à un site distant via PHP et le protocole POST. Bien sûr le forum est en anglais mais je suis certain que ce sera le fil d'Ariane concernant cette tâche.

### CORRECTION DU PROGRAMME !!!
Rien ne s'affichait sur le moniteur série; donc j'ai dû mettre un serial 9600 bauds au lieu de 112k bauds pour que l'on puisse mieux lire les infos.

# Bilan séance 01/03/2017
J'ai réfléchi à l'issue numéro #12, envoyé des données de localisation sur un serveur , j'ai trouvé cette solution mais il faudrait l'adapter pour notre projet
https://docs.labs.mediatek.com/resource/linkit-one/en/tutorials/using-http-with-the-linkit-one-development-board

### Attente/Travail par le suivant
Continu cette issue